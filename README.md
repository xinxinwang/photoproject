# README #

### What is this repository for? ###

* A sample Django Rest Framework (DRF) project to post photos and generate Restful API for Django models
* Provides a GUI to test the API:
      * A backend GUI for posts of photos using generated API (JSON and HTTP Rest API)
      * A login GUI for users to log in for creation, update and delete posts and photos
      * An admin GUI to manage users. 
* The project is based on the first part (DRF) of the online tutorial:

       http://blog.kevinastone.com/getting-started-with-django-rest-framework-and-angularjs.html.

This tutorial seems to use the older version of Django. I have to make changes to make it work for Djiango 1.7 in pycharm IDE. I also  added code to explore more features of DRF. I have not added the second part (Angular JS) to the project

### How do I get set up? ###

* Install Pillow in your Python environment: 

          pip install Pillow

* Download a copy of  PhotoProject to your local machine: 

          git remote add origin https://xinxinwang@bitbucket.org/xinxinwang/PhotoProject.git

* Open the project in  PyCharm Professional Edition
* Run the projects on Django Server
* Open http://127.0.0.1:8000/.
* To log in, click Log-In or open http://127.0.0.1:8000/api-auth/login/. Three users were already added: 

        admin/admin  (super user), 
        xinxin/xinxin,
        joe/joe

* To manage users, open http://127.0.0.1:8000/admin/ and log in as admin/admin

### Additional Notes ###
*  I already uploaded some pictures to the DB.
* All the source files for the API GUI are in the backend  folder
* I set up a frontend app and copy angular.js there, but has not added any code.