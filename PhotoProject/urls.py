from django.conf.urls import patterns, include, url
from django.contrib import admin
from backend import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', views.UserViewSet, 'photouser')
router.register(r'posts', views.PostViewSet, 'post')
router.register(r'photos', views.PhotoViewSet, 'photo')

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]