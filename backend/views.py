from rest_framework import generics, permissions
from rest_framework.decorators import api_view, detail_route, list_route
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework import viewsets

from backend.permissions import IsAuthorOrReadOnly


from .serializers import UserSerializer, PostSerializer, PhotoSerializer
from .models import PhotoUser, Post, Photo

@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'posts': reverse('post-list', request=request, format=format),
        'photos': reverse('photo-list', request=request, format=format),
    })

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = PhotoUser.objects.all()
    serializer_class = UserSerializer
    #lookup_field = 'username'
    permission_classes = [
        permissions.AllowAny
    ]


class PostViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `user_posts` action.
    """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        IsAuthorOrReadOnly,
    ]


    @detail_route()
    def byuser(self, request, *args, **kwargs):
        queryset = Post.objects.filter(author__id=kwargs.get('pk'))
        serializer = PostSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    def pre_save(self, obj):
        """Force author to the current user on save"""
        obj.author = PhotoUser.objects.get(id=self.request.user.id)


class PhotoViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `post_photos` action.
    """
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer

    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        IsAuthorOrReadOnly,
    ]

    @detail_route()
    def bypost(self, request, *args, **kwargs):
        queryset = Photo.objects.filter(post__pk=self.kwargs.get('pk'))
        serializer = PhotoSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @detail_route(renderer_classes=[renderers.TemplateHTMLRenderer, renderers.JSONRenderer, renderers.JSONPRenderer])
    def image(self, request, *args, **kwargs):
        queryset = Photo.objects.get(id=self.kwargs['pk'])
        serializer = PhotoSerializer(queryset, context={'request': request})
        if request.accepted_renderer.format == 'json' or request.accepted_renderer.format == 'jsonp':
            return Response(serializer.data)
        return Response({'data': serializer.data}, template_name='photo.html')

    def pre_save(self, obj):
        """ set owner and add followees to the users """
        obj.owner = PhotoUser.objects.get(id=self.request.user.id)
        obj.post.author.followers.add(obj.owner)

