import os
from django.conf import settings
from rest_framework import serializers

from .models import Post, Photo, PhotoUser



class UserSerializer(serializers.HyperlinkedModelSerializer):
    posts = serializers.HyperlinkedIdentityField('posts', view_name='post-byuser')
    #posts = serializers.HyperlinkedIdentityField('posts', view_name='post-byuser', lookup_field='username')
    followers = serializers.SlugRelatedField(many=True, slug_field='username')

    class Meta:
        model = PhotoUser
        fields = ('url', 'id', 'username', 'first_name', 'last_name', 'posts', 'followers')

class PostSerializer(serializers.HyperlinkedModelSerializer):
    #author = UserSerializer(required=False)
    author = serializers.Field(source='author.username')
    photos = serializers.HyperlinkedIdentityField('photos', view_name='photo-bypost')

    class Meta:
        model = Post
        fields = ('url', 'id', 'created', 'author', 'title', 'body', 'photos')


class PhotoSerializer(serializers.HyperlinkedModelSerializer):
    #post = serializers.HyperlinkedRelatedField(view_name='post-detail')
    post = serializers.SlugRelatedField(slug_field='title')
    owner = serializers.Field(source='owner.username')
    #file = serializers.Field(source='image.url')
    view = serializers.HyperlinkedIdentityField(view_name='photo-image')

    class Meta:
        model = Photo
        fields = ('url', 'id', 'post', 'owner', 'image', 'view')

    def transform_image(self, obj, value):
        """ convert image file to url """
        return os.path.join("/", value)

    #def validate(self, attrs):
    #    """ checks """
    #    return attrs

