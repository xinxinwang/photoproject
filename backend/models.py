from django.core.files.storage import FileSystemStorage
from django.db import models, backend
#from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User

class PhotoUser(User):
    followers = models.ManyToManyField('self', related_name='followees', symmetrical=False)

class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    #author = models.ForeignKey('auth.User', related_name='posts')
    author = models.ForeignKey(PhotoUser, related_name='posts')
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('created',)

class Photo(models.Model):
    post = models.ForeignKey(Post, related_name='photos')
    owner = models.ForeignKey(PhotoUser, related_name='photos', )
    image = models.ImageField(upload_to="media/%Y/%m/%d")
