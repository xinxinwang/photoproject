from django.core.files import File
from django.test import TestCase
from django.conf import settings
from rest_framework.renderers import JSONRenderer, StaticHTMLRenderer
from rest_framework.parsers import JSONParser
from rest_framework.compat import BytesIO
import os

from backend.models import PhotoUser, Post, Photo
from backend.serializers import UserSerializer, PhotoSerializer, PostSerializer

IMG_DIR = os.path.join(settings.BASE_DIR, "media/test/")

class PostTestCase(TestCase):

    def create_photo(self, post, file_name):
        img_url = os.path.join(IMG_DIR, file_name)
        with open(img_url, 'rb') as f:
            file = File(f)
            photo = Photo(post=post, image=file)
            photo.save()

    def setUp(self):
        author = PhotoUser(id=1, username='author 1')
        author.save()

        post = Post(author=author, title='post 1', body='body 1')
        post.save()

        #self.create_phone(post, 'Completed.jpg')
        #self.create_phone(post, 'In-Progress.jpg')

        post = Post(author=author, title='post 2', body='body 2')
        post.save()

        author = PhotoUser(id=2, username='author 2')
        author.save()

        post = Post(author=author, title='Post 3', body='body 3')
        post.save()

        #self.create_phone(post, 'To-Be-Reviewed.jpg')
        print("*********** SetUp done")

    def test_serializer(self):
        author = PhotoUser.objects.get(id=1)
        print("*********** Author: ", author.username)

        #serializer = UserSerializer(author)
        #print("*********** User Serializer: ", serializer.data)

        posts = Post.objects.all()
        print("*********** # of All Posts: ", len(posts))
        if len(posts) > 0:
            print("*********** Post[0]: '%s' by author '%s'" % (posts[0].title, posts[0].author))


        posts = author.posts.all()
        print("*********** # of Posts by author '%s': %d" % (author.username, len(posts)))

        photos = Photo.objects.all()
        print("*********** # of All Photos: ", len(photos))
        if len(photos) > 0:
            print("*********** Photo[0]: '%s' by post '%s'" % (photos[0].image, photos[0].post.title))

        if len(posts) > 0:
            photos = posts[0].photos.all()
            print("*********** # of photos by post '%s': %d" % (posts[0].title, len(posts)))

        """
        serializer = PostSerializer(posts[0])
        print("Serialized post:", serializer.data)

        # serializer = PostSerializer(author.posts.all(), many=True, context={'request': self.request})
        print("Serialized posts:", serializer.data)

        content = JSONRenderer().render(serializer.data)
        print("Serialized JSON:", content)

        content = StaticHTMLRenderer().render(serializer.data)
        print("Serialized HTML:", content)
        """
